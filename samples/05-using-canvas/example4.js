var
	fedo_resource = '../../assets/animations/federico.png',
	coin_resource = '../../assets/animations/coin.png',
	bg_resource   = '../../assets/images/bg-pixelized.jpg',
	GUY_STAY      = 0,
	GUY_RIGHT     = 1,
	GUY_TALK      = 2,
	GUY_LEFT      = 3,
	GUY_SEE_RIGHT = 0,
	GUY_SEE_LEFT  = 1;


var preloader = new Preloader({end: initGame});

preloader.enqueue([fedo_resource, coin_resource, bg_resource]);

function initGame() {
	var game         = new Game(),
	    drawer       = new GameDrawer(),
	    mouse        = new MouseMapper(),
	    controlPanel = new ControlPanel(),
	    actionInfo   = new ActionInfo(),
	    behaviors    = new Behaviors();
	drawer.createCanvas(false, 1800, 950);
	/*
	 * Registering the background image
	 */
	console.log(game.getCanvasSize());
	game.createGameObject('Sky')
	    .debugable(true)
	    .setZIndex(999)
	    .register(preloader.getResource(bg_resource))
	    .setPosition(0, 0)
	    //.setFillMode(GameObject.FillMode.FULL_WIDTH)
	    .setSize(1857, 743)
	    .setFPS(1)
	    .setAllowedSteps(0)
	    .setCurrentStep(0)
	    .setDirection(0)
	    .setCurrentStep(0);

	/*
	 * Registering the coin
	 */
	game.createGameObject('Coin')

	    // Loading asset and defining sprites size
	    .register(preloader.getResource(coin_resource))
	    .setSize(64, 64)

	    // Coin has just one animation. The first registered animation is the default used animation if not else defined
	    .registerAnimation('coin', 0, 0, 10, 10)

	    // Locate on the screen and defining zoom and zIndex level. Greater level means early render.
	    .setPosition(-150, -270)
	    .scaleTo(50)
	    .setZIndex(100)

	    // Defining some custom properties
	    .setProperty('label', 'la moneta')
	    .setProperty('behaviors', {
		    'examine': function () {
			    var fedo = this.pushAnimationState()
			                   .useAnimation(GUY_TALK);
			    behaviors.talk('Wow... un doblone...', 1500, fedo.popAnimationState);
			    return false;
		    },
		    'pickup':  function () {
			    var fedo = this.pushAnimationState();

			    if (fedo.isNear('Coin', 100, 0)) {

				    behaviors.talk('ehm... mi sembra di cioccolata...', 2000, fedo.popAnimationState);
				    return false;

			    } else {

				    behaviors.talk('È troppo distante, forse sarebbe il caso di avvicinarmi', 2500, fedo.popAnimationState);
				    return false;
			    }

		    }
	    });


	game.createGameObject('Fedo')
	    .setZIndex(101)
	    .register(preloader.getResource(fedo_resource))
	    .setSize(200, 360)

	    .registerAnimation(GUY_STAY + '@right', GUY_STAY, GUY_SEE_RIGHT, 0, 10)
	    .registerAnimation(GUY_STAY + '@left', GUY_STAY, GUY_SEE_LEFT, 0, 10)
	    .registerAnimation(GUY_TALK, GUY_TALK, 0, 6, 10)
	    .registerAnimation(GUY_LEFT, GUY_LEFT, 0, 6, 10)
	    .registerAnimation(GUY_RIGHT, GUY_RIGHT, 0, 6, 10)

	    .setPosition(100, -580)
	    .useAnimation(GUY_STAY + '@right')
	    .onSetState(function () {
		    var currentPosition = this.getPosition(),
		        step            = this.getProperty('step'),
		        targetX         = this.getProperty('targetX');
		    if (this.isAnimation(GUY_LEFT) || this.isAnimation(GUY_RIGHT)) {
			    if (currentPosition.x < targetX && step > 0 || currentPosition.x > targetX && step < 0) {
				    this.updatePosition(step * 8, 0);
			    } else {

				    if (step > 0) {
					    this.useAnimation(GUY_STAY + '@right');
				    } else {
					    this.useAnimation(GUY_STAY + '@left');
				    }

			    }
		    }
	    })

	    // .debugable(true)
	    .setProperty('label', 'me stesso')
	    .setProperty('behaviors', {
		    'talk':    function () {

			    game.getGameObjectByIdentifier('Fedo')
			        .pushAnimationState()
			        .useAnimation(GUY_TALK);
			    behaviors
				    .talk('Non posso parlare con me stesso, non saprei cosa domandarmi... E non saprei neanche se la risposta che mi sto dando è corretta',
				          5000,
				          game.getGameObjectByIdentifier('Fedo').popAnimationState);
			    return false;
		    },
		    'examine': function () {

			    game.getGameObjectByIdentifier('Fedo')
			        .pushAnimationState()
			        .useAnimation(GUY_TALK);

			    behaviors
				    .talk('Hai mai visto un personaggio più pixelloso di me? Scommetto proprio di no!', 3000,
				          game.getGameObjectByIdentifier('Fedo').popAnimationState);
			    return false;
		    }
	    });

	controlPanel.init();
	actionInfo.init();
	behaviors.init();
	game.start();

	// return;

	function movePlayer(event) {

		console.warn(event.offsetX, event.offsetY);
		var fedo     = game.getGameObjectByIdentifier('Fedo'),
		    position = fedo.getPosition(),
		    targetX  = event.offsetX,
		    delta    = Math.abs(position.x - targetX),
		    step     = 0;


		if (delta > 100) {
			if (position.x < targetX) {
				fedo.useAnimation(GUY_RIGHT);
				step = 1;
			} else {
				fedo.useAnimation(GUY_LEFT);
				step = -1;
			}
		}
		fedo.setProperty('delta', delta);
		fedo.setProperty('targetX', targetX);
		fedo.setProperty('step', step);

	}

	function hilightItem(x, y, button) {

		var objects = game.getGameObjectsByCoords(x, y, 998);


		if (objects.length > 0) {


			for (var i = 0; i < objects.length; i++) {
				if (objects[i].getProperty('is-button')) {

					controlPanel.resetHoverStatus();

					objects[i].setProperty('is-hover', true);
					if (button !== 0) {
						controlPanel.setCurrentAction(objects[i].getProperty('button-info'));

						return false;
					}
					//
				} else if(objects.length === 1 || objects[i].getIdentifier() !== 'Fedo'){
					actionInfo.use(objects[i]);

					if (button !== 0) {

						var objectBehaviors = objects[i].getProperty('behaviors');

						if (objectBehaviors) {
							var actionId = controlPanel.getCurrentAction().id;

							if (objectBehaviors[actionId]) {

								console.log(objects[i].getIdentifier(), actionId, objectBehaviors);
								behaviors.bindTo(game.getGameObjectByIdentifier('Fedo'));
								return objectBehaviors[actionId].call(game.getGameObjectByIdentifier('Fedo'));
							}
						}

					}
				}else{
					console.log(objects.map(function (o) {
						return o.getIdentifier();
					}));
				}
			}

		} else {
			actionInfo.use(null);
			controlPanel.resetHoverStatus();
		}
	}

	mouse.bind(MouseMapper.BUTTONS.LEFT,
	           undefined,
	           movePlayer)
	     .move(hilightItem);

}
