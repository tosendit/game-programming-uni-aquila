var
	myShip          = '../../assets/animations/my-ship.png',
	theWeapon       = '../../assets/animations/weapon.png',
	enemyShip       = '../../assets/animations/enemy-ship.png',
	stars           = '../../assets/images/stars.png',


	audioExplosion  = '../../assets/audio/explosion.mp3',
	audioWeapon     = '../../assets/audio/weapon.mp3',
	audioEngine     = '../../assets/audio/engine.mp3',
	audioBackground = '../../assets/audio/bgsound.mp3';

var preloader = new Preloader({end: initGame});

var SHIP_SPEED   = 3,
    WEAPON_SPEED = 5;

preloader.enqueue([myShip, enemyShip, stars, theWeapon]);

function initGame() {
	var game             = new Game(),
	    drawer           = new GameDrawer(),
	    collisionManager = new GameCollider(),
	    keyboard         = new KeyMapper();

	var bgSound = new Audio(audioBackground);
	bgSound.volume = 0.5;
	bgSound.loop = true;
	bgSound.play();

	var engineSound = new Audio(audioEngine);
	engineSound.volume = 0.1;
	engineSound.play();


	drawer.createCanvas(false, 800, 500);
	/*
	 * Registering the background image
	 */
	game.setCollisionManager(collisionManager);

	game.createGameObject('Sky')
	    .register(preloader.getResource(stars))
	    .setSize(100, 100)
	    .setFPS(30)
	    .setZIndex(999)
	    .setPosition(0, 0)
	    .setAllowedSteps(0)
	    .setDirection(0)
	    .setCurrentStep(0)
	    .setFillMode(GameObject.FillMode.REPEAT_ALL)
	    .onSetState(function () {
		    this.updatePosition(0, 2);
	    });

	// Energy & Weapons indicators

	game.createGameObject('energyIndicator')
	    .setFPS(10)
	    .setZIndex(200)
	    .setPosition(0, 0)
	    .setDirection(0)
	    .setCurrentStep(0)
	    .onRender(function (fps, item, context) {
		    context.fillStyle = 'yellow';
		    context.font = '50pt Helvetica';
		    context.shadowColor = 'black';

		    var enemy = game.getGameObjectByIdentifier('enemyShip');
		    context.fillText(enemy.getProperty('health') + '%', 10, 100);
	    }, true);


	game.createGameObject('weaponCount')
	    .setFPS(10)
	    .setZIndex(200)
	    .setPosition(0, 0)
	    .setDirection(0)
	    .setCurrentStep(0)
	    .onRender(function (fps, item, context) {
		    context.fillStyle = 'yellow';
		    context.font = '50pt Helvetica';
		    context.shadowColor = 'black';

		    var theShip = game.getGameObjectByIdentifier('myShip');
		    context.fillText(theShip.getProperty('weapons'), 10, 200);
	    }, true);


	/*
	 * Registering the coin
	 */
	game.createGameObject('myShip')

	    // Loading asset and defining sprites size
	    .register(preloader.getResource(myShip))
	    .setSize(100, 100)

	    .registerAnimation('front', 0, 0, 3, 20)
	    .registerAnimation('right', 1, 0, 3, 20)
	    .registerAnimation('left', 2, 0, 3, 20)

	    .useAnimation('front')
	    .setPosition(350, -120)
	    .setZIndex(100)

	    .setProperty('weapons', 0)
	    .onSetState(function () {
		    if (this.isAnimation('left')) {
			    this.updatePosition(SHIP_SPEED, 0);
		    } else if (this.isAnimation('right')) {
			    this.updatePosition(-SHIP_SPEED, 0);
		    }
	    });

	game.createGameObject('enemyShip')
	    .register(preloader.getResource(enemyShip))
	    .setSize(100, 100)

	    .registerAnimation('front', 0, 0, 3, 20)
	    .registerAnimation('right', 1, 0, 3, 20)
	    .registerAnimation('left', 2, 0, 3, 20)
	    .collidable()
	    .useAnimation('front')
	    .setPosition(350, 50)
	    .setZIndex(100)

	    .setProperty('moveToX', false)
	    .setProperty('health', 100)
	    .onSetState(function () {
		    var currentX = this.getPosition().x,
		        moveToX  = this.getProperty('moveToX');

		    if (moveToX && currentX > moveToX && (currentX - moveToX) / SHIP_SPEED > SHIP_SPEED) {

			    this.updatePosition(-SHIP_SPEED, 0);
			    if (!this.isAnimation('right')) this.useAnimation('right');
		    } else if (moveToX && currentX < moveToX && (moveToX - currentX) / SHIP_SPEED > SHIP_SPEED) {
			    this.updatePosition(SHIP_SPEED, 0);
			    if (!this.isAnimation('left')) this.useAnimation('left');
		    } else {
			    if (!this.isAnimation('front')) this.useAnimation('front');

			    if (Math.random() > 0.8) {
				    this.setProperty('moveToX', parseInt((Math.random() * (game.getCanvasSize().width - this.getSize().width)) / 3) * 3);
			    }
		    }
	    });

	game.start();

	var moveOnLeft  = false,
	    moveOnRight = false;

	function updateShip() {
		if (moveOnLeft && !moveOnRight) {
			game.getGameObjectByIdentifier('myShip').useAnimation('left');
		} else if (!moveOnLeft && moveOnRight) {
			game.getGameObjectByIdentifier('myShip').useAnimation('right');
		} else {
			game.getGameObjectByIdentifier('myShip').useAnimation('front');
		}
	}

	keyboard.bind('d', function () {
		              moveOnLeft = true;
		              updateShip();
	              },
	              function () {
		              moveOnLeft = false;
		              updateShip();
	              })
	        .bind('a', function () {
		              moveOnRight = true;
		              updateShip();
	              },
	              function () {
		              moveOnRight = false;
		              updateShip();
	              })
	        .bind('w', function () {

		        var weapon = game.getGameObjectByIdentifier('weapon');
		        if (typeof weapon === "undefined") {


			        var ship    = game.getGameObjectByIdentifier('myShip'),
			            shipX   = ship.getPosition().x,
			            weapons = ship.getProperty('weapons');

			        ship.setProperty('weapons', weapons + 1);
			        // Play sound weapon fired
			        var weaponSound = new Audio(audioWeapon);
			        weaponSound.play();

			        game.createGameObject('weapon')
			            .register(preloader.getResource(theWeapon))
			            .setSize(29, 44)
			            .setZIndex(100)
			            .collidable()
			            .setPosition(shipX, -120)
			            .registerAnimation('firing', 0, 0, 3, 50)
			            .onSetState(function () {
				            this.updatePosition(0, -WEAPON_SPEED);
				            var enemyShip = game.getGameObjectByIdentifier('enemyShip');
				            if (this.collidedWith(enemyShip)) {
					            enemyShip
						            .setProperty('moveToX', false)
						            .setProperty('health', enemyShip.getProperty('health') - 10);

					            this.remove();
					            // Play Sound explosion

					            var explosionSound = new Audio(audioExplosion);

					            explosionSound.play();

				            } else if (this.getPosition().y < -29) {
					            this.remove();
				            }
			            });
		        }

	        });

}
