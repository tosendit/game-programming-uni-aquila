(function () {
	'use strict';

	var DEBUG = false;

	var game    = null,
	    paused  = true,
	    initAll = false;

	/**
	 * The Main Game object
	 *
	 * @constructor
	 */
	window.Game = function () {

		/**
		 * Singleton tecnique
		 */
		if (game !== null) return game;

		game = this;

		var animationFrameID    = null,
		    previousTime        = 0,
		    _renderCallbacks    = [],
		    _preRenderCallbacks = [];

		/**
		 * Register a callback function for the rendering step
		 *
		 * @param {function} callback the method to invoke before on after the rendering task according to next argument
		 * @param {boolean} onBackface if true the callback will be execute before the rendering task. Default value is false
		 *
		 * @return {Window.Game}
		 */
		this.onRender = function (callback, onBackface) {
			if (onBackface === true) {
				_preRenderCallbacks.push(callback);
			} else {
				_renderCallbacks.push(callback);
			}
			return this;
		};

		this.setCollisionManager = function (manager) {
			collisionManager = manager;
			return this;
		};

		/**
		 * Starts up the game
		 */
		this.start = function () {
			paused = false;

			animationFrameID = requestAnimationFrame(render);
			return this;
		};

		/**
		 * Suspends the animation loop
		 */
		this.pause = function () {
			// this.stop();
			cancelAnimationFrame(animationFrameID);

			paused = true;
			initAll = false;
		};

		/**
		 * Stops the animation loop
		 */
		this.stop = function () {

			cancelAnimationFrame(animationFrameID);

			paused = true;
			initAll = true;

		};

		function renderContext(time, context, isBackContext) {

		}

		this.getContext = function (backscreen) {
			return this;
		};


		/**
		 * Executes the rendering of the scene
		 * @param time
		 */
		this.render = function (time) {
			if (DEBUG) console.group('this.render'); /* this.render */

			var fps          = parseInt(1000 / (time - previousTime)),
			    backContext  = game.getContext(true),
			    frontContext = game.getContext(false);

			previousTime = time;

			if (DEBUG) console.group('this.render.backface'); /* this.render.backface */
			_preRenderCallbacks.forEach(function (callback) {
				if (typeof callback === "function") callback(fps, backContext);
			});
			renderContext(time, backContext, true);
			if (DEBUG) console.groupEnd(); /* this.render.backface */


			if (DEBUG) console.group('this.render.frontface'); /* this.render.frontface */
			renderContext(time, frontContext);
			_renderCallbacks.forEach(function (callback) {
				if (typeof callback === "function") callback(fps, frontContext);
			});
			if (DEBUG) console.groupEnd(); /* this.render.frontface */
			if (DEBUG) console.groupEnd(); /* this.render */

		};

		function render(time) {
			if (paused) return;

			game.render(time);
			animationFrameID = requestAnimationFrame(render);
		}
	};

})();