(function () {
	'use strict';

	/**
	 * Regulate the behavior of a generic Game Object
	 * @param item {Object|string} the Game Object unique identifier
	 *
	 * @constructor
	 */
	window.GameObject = function (item) {

		var _steps             = 30,
		    _id                = null,
		    _renderContext     = GameObject.RenderContext.FRONT,
		    _isPaused          = false,
		    _item              = null,
		    _fps               = 30, /* Default frame rate */
		    _step              = 0,
		    _width             = 0,
		    _height            = 0,
		    _x                 = 0,
		    _y                 = 0,
		    _direction         = 'front',
		    _framesDirection   = 'X',
		    _fillMode          = GameObject.FillMode.NONE,
		    previousTime       = 0,
		    _renderCallbacks   = [],
		    _setStateCallbacks = [],
		    _properties        = {},
		    _scale             = 100,
		    _zIndex            = 0,
		    _animationStatus   = [],
		    _animations        = [],
		    _animationName     = null,
		    _debugable         = false;


		/**
		 * Bind the GameObject to a specific unique identifier or another object
		 *
		 * @param item {string|Object} the Game Object unique identifier
		 * @return {Window.GameObject}
		 */
		this.register = function (item) {
			_item = item;
			return this;
		};

		this.setZIndex = function (zIndex) {
			_zIndex = zIndex;
			return this;
		};
		this.getZIndex = function () {
			return _zIndex || 0;
		};

		this.setProperty = function (propertyName, propertyValue) {
			_properties[propertyName] = propertyValue;
			return this;
		};

		this.getProperty = function (propertyName) {
			return _properties[propertyName];
		};

		this.getAllProperties = function () {
			return {
				properties:       {
					id:              _id,
					renderContext:   _renderContext,
					animationName:   _animationName,
					isPaused:        _isPaused,
					item:            _item,
					fps:             _fps,
					step:            _step,
					steps:           _steps,
					width:           _width,
					height:          _height,
					x:               _x,
					y:               _y,
					direction:       _direction,
					framesDirection: _framesDirection,
					fillMode:        _fillMode,
					scale:           _scale,
					zIndex:          _zIndex,
					debugable:       _debugable,
				},
				customProperties: _properties
			}
		};

		this.debugable = function (status) {

			if (status === undefined) {
				return _debugable;
			} else {
				_debugable = status;
				return this;
			}
		};

		this.setIdentifier = function (id) {
			_id = id;
			return this;
		};

		this.getIdentifier = function () {
			return _id;
		};

		this.registerAnimation = function (animationName, direction, currentStep, allowedSteps, fps) {
			_animations[animationName] = {
				currentStep:  currentStep,
				allowedSteps: allowedSteps,
				fps:          fps,
				direction:    direction
			};
			if (_animationName === null) this.useAnimation(animationName);
			return this;
		};

		this.useAnimation = function (animationName) {
			var info = _animations[animationName]
			_animationName = animationName;
			this.setAllowedSteps(info.allowedSteps)
			    .setCurrentStep(info.currentStep)
			    .setFPS(info.fps)
			    .setDirection(info.direction);
			return this;
		};

		this.isAnimation = function (usedAniamtion) {
			return _animationName === usedAniamtion;
		};


		/**
		 * Sets the animation frames in sprite image
		 *
		 * @param {string} mode can be 'X' or 'Y'
		 * @return {Window.GameObject}
		 */
		this.setFramesDirection = function (mode) {
			_framesDirection = mode;
			return this;

		};
		/**
		 * Returns the defined animation frame direction of the sprite image of this object
		 * @return {string}
		 */
		this.getFramesDirection = function () {
			return _framesDirection;
		};

		/**
		 * Returns the current step index of animation
		 * @return {number}
		 */
		this.getCurrentStep = function () {
			return isNaN(_step) ? 0 : _step;
		};

		/**
		 * Sets the step of current animation. If the step exceeds the number of max steps the number is set in the range
		 * @param value {number} the index of current animation
		 * @return {Window.GameObject}
		 */
		this.setCurrentStep = function (value) {
			if (_steps > 0) {
				_step = value % _steps;
			} else {
				_step = value;
			}
			return this;
		};


		/**
		 * Sets the limit to a specific animation index
		 * @param value
		 * @return {Window.GameObject}
		 */
		this.setAllowedSteps = function (value) {
			_steps = value;
			return this;
		};

		/**
		 * Returns the binded object
		 * @return {*}
		 */
		this.getObject = function () {
			return _item;
		};

		/**
		 * Set the max FPS of this object
		 * @param fps {number} the FPS limit
		 * @return {Window.GameObject}
		 */
		this.setFPS = function (fps) {
			_fps = fps;
			return this;
		};

		/**
		 * Get the FPS value defined for this GameObject
		 * @return {number}
		 */
		this.getFPS = function () {
			return _fps;
		};

		/**
		 * Set the object direction
		 *
		 * @param direction string an identifier for the direction
		 *
		 * @return {Window.GameObject}
		 */
		this.setDirection = function (direction) {
			_direction = direction;
			return this;
		};

		/**
		 * Get the object direction
		 * @return {string}
		 */
		this.getDirection = function () {
			return _direction;
		};


		this.setRenderContext = function (renderContext) {
			_renderContext = renderContext;
			return this;
		};

		this.getRenderContext = function () {
			return _renderContext;
		};


		this.setSize = function (width, height) {
			_width = width;
			_height = height;
			return this;
		};

		this.getSize = function (rawSize) {
			rawSize = rawSize === undefined ? true : rawSize;
			return {
				width:  _width / 100 * (rawSize ? 100 : _scale),
				height: _height / 100 * (rawSize ? 100 : _scale)
			};
		};

		this.scaleTo = function (percentage) {
			_scale = percentage;
			return this;
		};

		this.getScale = function () {
			return _scale;
		};

		this.setPosition = function (x, y) {
			_x = (x < 0) ? Game().getContext().canvas.width + x : x;
			_y = (y < 0) ? Game().getContext().canvas.height + y : y;
			return this;
		};

		this.updatePosition = function (x, y) {
			_x += x;
			_y += y;
			return this;
		};

		this.getPosition = function () {
			return {
				x: _x,
				y: _y
			};
		};

		this.setFillMode = function (mode) {

			_fillMode = mode;

			return this;
		};

		this.getFillMode = function () {
			return _fillMode;
		};


		/**
		 * Updates the state of current object
		 *
		 * @return {Window.GameObject}
		 */
		this.setState = function () {
			/* when overrided, the dev can inject a customized behavior */
			var theGameObject = this;
			_setStateCallbacks.forEach(function (setStateCallback) {
				if (typeof setStateCallback === 'function') {
					setStateCallback.apply(theGameObject);
				}
			});
			return this;
		};

		/**
		 *
		 * @param callback
		 * @return {Window.GameObject}
		 */
		this.onSetState = function (callback) {
			_setStateCallbacks.push(callback);
			return this;
		};

		/**
		 *
		 * @return {Window.GameObject}
		 */
		this.removeSetStateHandlers = function () {
			_setStateCallbacks = [];
			return this;
		};

		/**
		 * Initialize the object internal variables
		 *
		 * @param init is set to true when the game is in the initialization phase, when
		 */
		this.start = function (init) {
			/* when overrided, the dev can inject a customized behavior */
		};

		/**
		 * Set the object state to Paused
		 *
		 * @return {Window.GameObject}
		 */
		this.pause = function () {
			_isPaused = true;
			return this.setState();
		};


		this.isPaused = function () {
			return _isPaused;
		};

		/**
		 * Stop the object animation
		 */
		this.stop = function () {
			/* when overrided, the dev can inject a customized behavior */
		};

		/**
		 * Register a callback method invoked on the rendering step
		 *
		 * @param callback
		 *
		 * @return {Window.GameObject}
		 */
		this.onRender = function (callback) {

			_renderCallbacks.push(callback);
			return this;
		};

		/**
		 * Executes the object rendering if in right FPS.
		 * NOTE: This method is invoked by the Game object.
		 *
		 * @param time
		 * @param {HTMLCanvasElement} context the Execution canvas 2D context
		 */
		this.render = function (time, context) {

			var fps = parseInt(1000 / (time - previousTime));

			if (fps <= _fps) {

				if (_steps !== 0) {
					_step += 1;
					_step %= _steps;
				}

				this.setState();
				previousTime = time;
			}
			var theGameObject = this;
			_renderCallbacks.forEach(function (callback) {

				if (typeof callback === "function") callback.call(theGameObject, fps, _item, context);

			});

		};

		/**
		 * Check if this object is nearby another GameObject
		 * @param {GameObject|string} go the nearby GameObject identifier ( string ) or an instance of the GameObject
		 * @param {number} distance the maximum distance expressed in pixels between the objects
		 * @param {number} axes if 0 the check is performed only on X Axes, if 1 the check is performed only on Y axes,
		 *                  for any other value the check will determine the minor real distance between objects.
		 */
		this.isNear = function (go, distance, axes) {
			if (!(go instanceof GameObject)) {
				go = Game().getGameObjectByIdentifier(go);
			}
			var goPos  = go.getPosition(),
			    goSize = go.getSize(false),
			    myPos  = this.getPosition(),
			    mySize = this.getSize(false),
			    x1     = (axes === 1) ? 0 : (myPos.x + mySize.width / 2),
			    y1     = (axes === 0) ? 0 : (myPos.y + mySize.height / 2),
			    x2     = (axes === 1) ? 0 : (goPos.x + goSize.width / 2),
			    y2     = (axes === 0) ? 0 : (goPos.y + goSize.height / 2),
			    pd     = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));


			return pd <= distance;

		};


		this.pushAnimationState = function () {

			var props = this.getAllProperties();

			_animationStatus.push(props);

			return this;
		};

		this.popAnimationState = function () {
			var status = _animationStatus.pop();
			if (status) {
				var props = status.properties;

				_id = props.id;

				_direction = props.direction;
				_animationName = props.animationName;
				_step = props.step;
				_steps = props.steps;
				_isPaused = props.isPaused;

				_item = props.item;
				_fps = props.fps;
				_width = props.width;
				_height = props.height;
				_framesDirection = props.framesDirection;
				_fillMode = props.fillMode;
				_scale = props.scale;
				_zIndex = props.zIndex;
				_debugable = props.debugable;

			}
		};

	};

	GameObject.RenderContext = {
		DISABLED: 0,
		FRONT:    1,
		BACK:     2,
	};

	GameObject.FillMode = {
		NONE:        0,
		REPEAT_X:    1,
		REPEAT_Y:    2,
		REPEAT_ALL:  3,
		FULL_WIDTH:  4,
		FULL_HEIGHT: 5,
		STRETCH:     6
	};

})();

