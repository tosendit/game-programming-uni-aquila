(function () {


	var bindedTo         = null,
	    displayedMessage = '';

	window.Behaviors = function () {

		this.talk = function (message, timeout, then) {

			displayedMessage = message;
			setTimeout(function () {
				displayedMessage = '';

				if(typeof then === 'function') then();

			}, timeout);

		};

		this.default = function (message, timeout) {
			this.talk(message, timeout);
		};


		this.bindTo = function (gameObject) {
			bindedTo = gameObject;
			return this;
		};

		this.init = function () {

			var game = Game();
			game.createGameObject('talkTo')
			    .setPosition(0, 0)
			    .setSize(1, 1)
			    .onRender(function (fps, item, context) {
				    if (displayedMessage !== '') {

					    context.fillStyle = 'yellow';
					    context.font = '18pt Helvetica';
					    context.shadowColor = 'black';

					    var lineHeight  = 30,
					        position    = bindedTo.getPosition(),
					        lines       = GameDrawer().wrapText(context, displayedMessage,
					                                            position.x, position.y,
					                                            context.canvas.width - position.x - 50, lineHeight),
					        startOffset = -(lines.length - 1) * lineHeight;


					    for(var i = 0; i < lines.length; i++ ){
						    var line = lines[i];
						    console.log(line);
						    context.shadowOffsetX = 2;
						    context.shadowOffsetY = 2;
						    context.shadowBlur = 2;
					    	context.fillText( lines[i].text, lines[i].x, lines[i].y + startOffset );

					    }


				    }
			    });
		};


	}

})();