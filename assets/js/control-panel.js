(function () {

	var instance      = null,
	    currentAction = null;


	window.ControlPanel = function () {

		if (instance !== null) return instance;


		instance = this;

		var gameInstance = Game(),
		    functions    = [

			    {
				    id:    'examine',
				    label: 'Esamina',
				    x:     0,
				    y:     0,
			    },
			    {
				    id:    'talk',
				    label: 'Parla con',
				    x:     1,
				    y:     0,
			    },
			    {
				    id:    'use',
				    label: 'Usa',
				    x:     2,
				    y:     0,
			    },
			    {
				    id:    'open',
				    label: 'Apri',
				    x:     0,
				    y:     1,
			    },
			    {
				    id:    'close',
				    label: 'Chiudi',
				    x:     1,
				    y:     1,
			    },
			    {
				    id:    'give',
				    label: 'Dai',
				    x:     2,
				    y:     1,
			    },
			    {
				    id:    'pull',
				    label: 'Tira',
				    x:     0,
				    y:     2,
			    },
			    {
				    id:    'push',
				    label: 'Spingi',
				    x:     1,
				    y:     2,
			    },
			    {
				    id:    'pickup',
				    label: 'Raccogli',
				    x:     2,
				    y:     2,
			    },

		    ];


		var GameButton = function (buttonInfo) {
			var gameObject    = new GameObject(),
			    fullWidth     = ControlPanel.BUTTON_WIDTH + ControlPanel.MARGIN * 2,
			    fullHeight    = ControlPanel.BUTTON_HEIGHT + ControlPanel.MARGIN * 2,
			    layoutOffsetX = buttonInfo.x * fullWidth,
			    layoutOffsetY = (-3 + buttonInfo.y) * fullHeight;


			gameObject
				.setProperty('is-button', true)
				.setProperty('is-hover', false)
				.setProperty('action-function', buttonInfo.action)
				.setProperty('button-info', buttonInfo)
				.setPosition(layoutOffsetX, layoutOffsetY)
				.setSize(fullWidth, fullHeight)
				.setIdentifier(buttonInfo.id)
				.onRender(function (fps, image, context) {
					var position = this.getPosition(),
					    size     = this.getSize();
					context.beginPath();
					context.strokeStyle = this.getProperty('is-hover') ? '#FF0' : '#FFF';
					context.fillStyle = context.strokeStyle;
					context.font = "20px 'Purisa'";
					context.fontStretch = '800';
					context.strokeRect(position.x + ControlPanel.MARGIN, position.y + ControlPanel.MARGIN, ControlPanel.BUTTON_WIDTH, ControlPanel.BUTTON_HEIGHT);
					context.fillText(buttonInfo.label, position.x + 5, position.y + ControlPanel.MARGIN + ControlPanel.BUTTON_HEIGHT - 20, ControlPanel.BUTTON_WIDTH - 10);

				}, true);

			gameInstance.registerObject(gameObject);

			return gameObject;
		};


		this.functions = functions;

		this.getCurrentAction = function () {
			if (currentAction === null) {
				currentAction = functions[0];
			}
			return currentAction;
		};

		this.setCurrentAction = function (object) {
			currentAction = object;
		};

		this.resetHoverStatus = function () {
			functions.forEach(function (item) {
				item.drawer.setProperty('is-hover', false);
			});
		};

		this.init = function () {

			var cpX      = 0,
			    cpY      = (new Game()).getCanvasSize().height - (ControlPanel.BUTTON_HEIGHT + ControlPanel.MARGIN * 2) * 3,
			    cpWidth  = (ControlPanel.BUTTON_WIDTH + ControlPanel.MARGIN * 2) * 3,
			    cpHeight = (ControlPanel.BUTTON_HEIGHT + ControlPanel.MARGIN * 2) * 3;

			gameInstance.createGameObject('bg-control-panel')
			            .setZIndex(9999)
			            .setPosition(cpX, cpY)
			            .setSize(cpWidth, cpHeight)
			            .debugable(true)
			            .onRender(function (fps, item, context) {
				            // console.log(cpX, cpY, cpWidth, cpHeight);
				            context.fillStyle = '#000';
				            context.rect(cpX, cpY, cpWidth, cpHeight);
				            // console.log('rendering rect', context.fillStyle);
				            context.fill();


			            }, true);

			functions.forEach(function (item) {
				item.drawer = new GameButton(item);
			});

		}
	};

	window.ControlPanel.BUTTON_WIDTH = 200;
	window.ControlPanel.BUTTON_HEIGHT = 50;
	window.ControlPanel.MARGIN = 2;

})();