(function () {
	'use strict';

	var DEBUG = false;

	var game    = null,
	    paused  = true,
	    initAll = false;


	/*
	 * =======================================================================
	 * =======================================================================
	 * =======================================================================
	 * =======================================================================
	 */


	/**
	 * The Main Game object
	 *
	 * @constructor
	 */
	window.Game = function () {

		if (game !== null) return game;

		game = this;

		var registeredObjects   = [],
		    zIndexObjects       = [],
		    animationFrameID    = null,
		    previousTime        = 0,
		    collisionManager    = null,
		    _renderCallbacks    = [],
		    _preRenderCallbacks = [],
		    _onRegisterObject   = null;

		this.canvas = {
			width:  0,
			height: 0
		};

		this.createGameObject = function (identifier) {
			var o = new GameObject();
			o.setIdentifier(identifier);
			this.registerObject(o);
			return o;

		};

		this.removeGameObject = function (identifier) {

			registeredObjects = registeredObjects.filter(function (gameObject) {
				return gameObject.getIdentifier() !== identifier
			});

		};

		/**
		 * Return a previously created object by its identifier
		 *
		 * @param identifier
		 * @return {Window.GameObject}
		 */
		this.getGameObjectByIdentifier = function (identifier) {

			for (var object of registeredObjects) {
				if (object.getIdentifier() === identifier) {
					return object;
				}
			}

		};

		this.updateZIndex = function () {
			zIndexObjects = [];
			for (var object of registeredObjects) {
				zIndexObjects[object.getZIndex()] = true;
			}
			return this;
		};

		this.registerObjects = function (objects) {

			objects.forEach(this.registerObject);
			return this;
		};

		/**
		 * Registe a new GameObject in the queue
		 *
		 * @param object GameObject
		 *
		 * @return {Game}
		 */
		this.registerObject = function (object) {

			if (typeof _onRegisterObject === "function") _onRegisterObject(object);
			registeredObjects.push(object);


			// Set the level of the object
			var zIndex = object.getZIndex() || 0;

			zIndexObjects[zIndex] = true;
			console.log("Registering object", object, zIndexObjects);
			return this;
		};

		/**
		 * Register a callback to invoke over the object when registered
		 *
		 * @param {function} callback
		 *
		 * @return {Game}
		 */
		this.onRegisterObject = function (callback) {
			_onRegisterObject = callback;

			return this;
		};


		this.getObjects = function () {

			return registeredObjects;
		};


		/**
		 * Returns all the objects that are under the given coordinate
		 *
		 * @param x
		 * @param y
		 * @param excludeIdentifiers
		 * @return {Array}
		 */
		this.getGameObjectsByCoords = function (x, y, z, excludeIdentifiers) {

			excludeIdentifiers = excludeIdentifiers || [];

			return registeredObjects.filter(function (registeredObject) {
				var position = registeredObject.getPosition(),
				    size     = registeredObject.getSize(false),
				    id       = registeredObject.getIdentifier(),
				    zIndex   = registeredObject.getZIndex();

				if (excludeIdentifiers.indexOf(id) === -1) {
					if (position.x <= x && position.y <= y && position.x + size.width >= x && position.y + size.height >= y && (z === undefined || zIndex < z)) {

						return true;

					}
				}
			});

		};

		this.getCanvasSize = function () {
			return this.canvas;
		};

		this.setCanvasSize = function (width, height) {
			this.canvas.width = width;
			this.canvas.height = height;
			return this;
		};


		this.getContext = function (backscreen) {
			return this;
		};

		/**
		 * Register a callback function for the rendering step
		 *
		 * @param {function} callback the method to invoke before on after the rendering task according to next argument
		 * @param {boolean} onBackface if true the callback will be execute before the rendering task. Default value is false
		 *
		 * @return {Window.Game}
		 */
		this.onRender = function (callback, onBackface) {
			if (onBackface === true) {
				_preRenderCallbacks.push(callback);
			} else {
				_renderCallbacks.push(callback);
			}
			return this;
		};


		this.setCollisionManager = function (manager) {
			collisionManager = manager;
			return this;
		};

		/**
		 * Starts up the game
		 */
		this.start = function () {
			paused = false;

			registeredObjects.forEach(function (go) {

				go.start(initAll);
			});

			animationFrameID = requestAnimationFrame(render);
			return this;
		};

		/**
		 * Suspends the animation loop
		 */
		this.pause = function () {
			// this.stop();
			cancelAnimationFrame(animationFrameID);
			registeredObjects.forEach(function (go) {
				go.pause();
			});
			paused = true;
			initAll = false;
		};

		this.stop = function () {

			cancelAnimationFrame(animationFrameID);
			registeredObjects.forEach(function (go) {
				go.stop();
			});
			paused = true;
			initAll = true;

		};

		function renderContext(time, context, isBackContext) {

			var keys = Object.getOwnPropertyNames(zIndexObjects).reverse();
			delete (keys[0]);
			keys = Object.values(keys);

			keys.forEach(function (key) {
				var processingObjects = registeredObjects.filter(function (object) {
					return object.getZIndex().toString() === key.toString();
				});

				processingObjects.forEach(function (go) {
					var renderContext = go.getRenderContext();
					if (renderContext === GameObject.RenderContext.DISABLED ||
					    renderContext === GameObject.RenderContext.FRONT && isBackContext ||
					    renderContext === GameObject.RenderContext.BACK && !isBackContext) return;

					if (DEBUG) console.log(go.getIdentifier(), isBackContext);

					go.render(time, context);

				});
			});
		}

		this.render = function (time) {
			if (DEBUG) console.group('this.render');
			var fps          = parseInt(1000 / (time - previousTime)),
			    backContext  = game.getContext(true),
			    frontContext = game.getContext(false);

			previousTime = time;

			this.updateZIndex();

			if (DEBUG) console.group('this.render.backface');
			_preRenderCallbacks.forEach(function (callback) {
				if (typeof callback === "function") callback(fps, backContext);
			});

			renderContext(time, backContext, true);
			if (DEBUG) console.groupEnd();

			if (DEBUG) console.group('this.render.frontface');
			renderContext(time, frontContext);
			_renderCallbacks.forEach(function (callback) {
				if (typeof callback === "function") callback(fps, frontContext);
			});
			if (DEBUG) console.groupEnd();
			if (DEBUG) console.groupEnd();

		};

		function render(time) {
			if (paused) return;

			game.render(time);
			if (collisionManager) {

				collisionManager.check();

			}
			animationFrameID = requestAnimationFrame(render);

		}

	};

	/*
	 * =======================================================================
	 * =======================================================================
	 * =======================================================================
	 * =======================================================================
	 */


	/**
	 * Regulate the behavior of a generic Game Object
	 * @param item {Object|string} the Game Object unique identifier
	 *
	 * @constructor
	 */
	window.GameObject = function (item) {

		var _steps             = 30,
		    _id                = null,
		    _renderContext     = GameObject.RenderContext.FRONT,
		    _collidable        = false,
		    _isPaused          = false,
		    _item              = null,
		    _fps               = 30, /* Default frame rate */
		    _step              = 0,
		    _width             = 0,
		    _height            = 0,
		    _x                 = 0,
		    _y                 = 0,
		    _direction         = 'front',
		    _framesDirection   = 'X',
		    _fillMode          = GameObject.FillMode.NONE,
		    previousTime       = 0,
		    _renderCallbacks   = [],
		    _setStateCallbacks = [],
		    _properties        = {},
		    _scale             = 100,
		    _zIndex            = 0,
		    _animationStatus   = [],
		    _animations        = [],
		    _animationName     = null,
		    _debugable         = false;


		/**
		 * Bind the GameObject to a specific unique identifier or another object
		 *
		 * @param item {string|Object} the Game Object unique identifier
		 * @return {Window.GameObject}
		 */
		this.register = function (item) {
			_item = item;
			return this;
		};

		this.remove = function () {
			game.removeGameObject(this.getIdentifier());
		};

		this.setZIndex = function (zIndex) {
			_zIndex = zIndex;
			return this;
		};
		this.getZIndex = function () {
			return _zIndex || 0;
		};

		this.setProperty = function (propertyName, propertyValue) {
			_properties[propertyName] = propertyValue;
			return this;
		};

		this.getProperty = function (propertyName) {
			return _properties[propertyName];
		};

		this.getAllProperties = function () {
			return {
				properties:       {
					id:              _id,
					renderContext:   _renderContext,
					animationName:   _animationName,
					collidable:      _collidable,
					isPaused:        _isPaused,
					item:            _item,
					fps:             _fps,
					step:            _step,
					steps:           _steps,
					width:           _width,
					height:          _height,
					x:               _x,
					y:               _y,
					direction:       _direction,
					framesDirection: _framesDirection,
					fillMode:        _fillMode,
					scale:           _scale,
					zIndex:          _zIndex,
					debugable:       _debugable,
				},
				customProperties: _properties
			}
		};

		this.debugable = function (status) {

			if (status === undefined) {
				return _debugable;
			} else {
				_debugable = status;
				return this;
			}
		};

		this.setIdentifier = function (id) {
			_id = id;
			return this;
		};

		this.getIdentifier = function () {
			return _id;
		};

		this.registerAnimation = function (animationName, direction, currentStep, allowedSteps, fps) {
			_animations[animationName] = {
				currentStep:  currentStep,
				allowedSteps: allowedSteps,
				fps:          fps,
				direction:    direction
			};
			if (_animationName === null) this.useAnimation(animationName);
			return this;
		};

		this.useAnimation = function (animationName) {
			var info = _animations[animationName]
			_animationName = animationName;
			this.setAllowedSteps(info.allowedSteps)
			    .setCurrentStep(info.currentStep)
			    .setFPS(info.fps)
			    .setDirection(info.direction);
			return this;
		};

		this.isAnimation = function (usedAniamtion) {
			return _animationName == usedAniamtion;
		};


		/**
		 * Sets the animation frames in sprite image
		 *
		 * @param {string} mode can be 'X' or 'Y'
		 * @return {Window.GameObject}
		 */
		this.setFramesDirection = function (mode) {
			_framesDirection = mode;
			return this;

		};
		/**
		 * Returns the defined animation frame direction of the sprite image of this object
		 * @return {string}
		 */
		this.getFramesDirection = function () {
			return _framesDirection;
		};

		/**
		 * Returns the current step index of animation
		 * @return {number}
		 */
		this.getCurrentStep = function () {
			return isNaN(_step) ? 0 : _step;
		};

		/**
		 * Sets the step of current animation. If the step exceeds the number of max steps the number is set in the range
		 * @param value {number} the index of current animation
		 * @return {Window.GameObject}
		 */
		this.setCurrentStep = function (value) {
			if (_steps > 0) {
				_step = value % _steps;
			} else {
				_step = value;
			}
			return this;
		};


		/**
		 * Sets the limit to a specific animation index
		 * @param value
		 * @return {Window.GameObject}
		 */
		this.setAllowedSteps = function (value) {
			_steps = value;
			return this;
		};

		/**
		 * Returns the binded object
		 * @return {*}
		 */
		this.getObject = function () {
			return _item;
		};

		this.pushAnimationState = function () {

			var props = this.getAllProperties();

			_animationStatus.push(props);

			return this;
		};

		this.popAnimationState = function () {
			var status = _animationStatus.pop();
			if (status) {
				var props = status.properties;

				_id = props.id;

				_direction = props.direction;
				_animationName = props.animationName;
				_step = props.step;
				_steps = props.steps;
				_isPaused = props.isPaused;

				_collidable = props.collidable;
				_item = props.item;
				_fps = props.fps;
				_width = props.width;
				_height = props.height;
				_framesDirection = props.framesDirection;
				_fillMode = props.fillMode;
				_scale = props.scale;
				_zIndex = props.zIndex;
				_debugable = props.debugable;

			}


		};

		/**
		 * Set the max FPS of this object
		 * @param fps {number} the FPS limit
		 * @return {Window.GameObject}
		 */
		this.setFPS = function (fps) {
			_fps = fps;
			return this;
		};

		/**
		 * Get the FPS value defined for this GameObject
		 * @return {number}
		 */
		this.getFPS = function () {
			return _fps;
		};

		/**
		 * Set the object direction
		 *
		 * @param direction string an identifier for the direction
		 *
		 * @return {Window.GameObject}
		 */
		this.setDirection = function (direction) {
			_direction = direction;
			return this;
		};

		/**
		 * Get the object direction
		 * @return {string}
		 */
		this.getDirection = function () {
			return _direction;
		};


		this.setRenderContext = function (renderContext) {
			_renderContext = renderContext;
			return this;
		};

		this.getRenderContext = function () {
			return _renderContext;
		};

		/**
		 * Updates the state of current object
		 *
		 * @return {Window.GameObject}
		 */
		this.setState = function () {
			/* when overrided, the dev can inject a customized behavior */
			var theGameObject = this;
			_setStateCallbacks.forEach(function (setStateCallback) {
				if (typeof setStateCallback === 'function') {
					setStateCallback.apply(theGameObject);
				}
			});
			return this;
		};

		/**
		 *
		 * @param callback
		 * @return {Window.GameObject}
		 */
		this.onSetState = function (callback) {
			_setStateCallbacks.push(callback);
			return this;
		};

		/**
		 *
		 * @return {Window.GameObject}
		 */
		this.removeSetStateHandlers = function () {
			_setStateCallbacks = [];
			return this;
		};


		/**
		 * Initialize the object internal variables
		 *
		 * @param init is set to true when the game is in the initialization phase, when
		 */
		this.start = function (init) {
			/* when overrided, the dev can inject a customized behavior */
		};

		/**
		 * Set the object state to Paused
		 *
		 * @return {Window.GameObject}
		 */
		this.pause = function () {
			_isPaused = true;
			return this.setState();
		};


		this.isPaused = function () {
			return _isPaused;
		};

		/**
		 * Stop the object animation
		 */
		this.stop = function () {
			/* when overrided, the dev can inject a customized behavior */
		};


		/**
		 * Register a callback method invoked on the rendering step
		 *
		 * @param callback
		 *
		 * @return {Window.GameObject}
		 */
		this.onRender = function (callback) {

			_renderCallbacks.push(callback);
			return this;
		};

		/**
		 * Executes the object rendering if in right FPS.
		 * NOTE: This method is invoked by the Game object.
		 *
		 * @param time
		 * @param {HTMLCanvasElement} context the Execution canvas 2D context
		 */
		this.render = function (time, context) {

			var fps = parseInt(1000 / (time - previousTime));

			if (fps <= parseInt(_fps)) {

				if (_steps !== 0) {
					_step += 1;
					_step %= _steps;
				}

				this.setState();
				previousTime = time;
			}
			var theGameObject = this;
			_renderCallbacks.forEach(function (callback) {

				if (typeof callback === "function") callback.call(theGameObject, fps, _item, context);

			});

		};


		// Collision management

		this.collidable = function (state) {
			_collidable = (state === undefined) || state;
			return this;
		};

		this.canCollide = function () {
			return _collidable;
		};

		this.setSize = function (width, height) {
			_width = width;
			_height = height;
			return this;
		};

		this.getSize = function (rawSize) {
			rawSize = rawSize === undefined ? true : rawSize;
			return {
				width:  _width / 100 * (rawSize ? 100 : _scale),
				height: _height / 100 * (rawSize ? 100 : _scale)
			};
		};

		this.scaleTo = function (percentage) {
			_scale = percentage;
			return this;
		};

		this.getScale = function () {
			return _scale;
		};

		this.setPosition = function (x, y) {
			_x = (x < 0) ? Game().getContext().canvas.width + x : x;
			_y = (y < 0) ? Game().getContext().canvas.height + y : y;
			return this;
		};

		this.updatePosition = function (x, y) {
			_x += x;
			_y += y;
			return this;
		};

		this.getPosition = function () {
			return {
				x: _x,
				y: _y
			};
		};

		this.setFillMode = function (mode) {

			_fillMode = mode;

			return this;
		};

		this.getFillMode = function () {
			return _fillMode;
		};


		/**
		 * Check if this object is nearby another GameObject
		 * @param {GameObject|string} go the nearby GameObject identifier ( string ) or an instance of the GameObject
		 * @param {number} distance the maximum distance expressed in pixels between the objects
		 * @param {number} axes if 0 the check is performed only on X Axes, if 1 the check is performed only on Y axes,
		 *                  for any other value the check will determine the minor real distance between objects.
		 */
		this.isNear = function (go, distance, axes) {
			if (!(go instanceof GameObject)) {
				go = Game().getGameObjectByIdentifier(go);
			}
			var goPos  = go.getPosition(),
			    goSize = go.getSize(false),
			    myPos  = this.getPosition(),
			    mySize = this.getSize(false),
			    x1     = (axes === 1) ? 0 : (myPos.x + mySize.width / 2),
			    y1     = (axes === 0) ? 0 : (myPos.y + mySize.height / 2),
			    x2     = (axes === 1) ? 0 : (goPos.x + goSize.width / 2),
			    y2     = (axes === 0) ? 0 : (goPos.y + goSize.height / 2),
			    pd     = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));


			return pd <= distance;

		};

		/**
		 * Check if the given GameObject collides with this GameObject
		 *
		 * @param {Window.GameObject} otherObject
		 */

		this.collidedWith = function (otherObject) {

			var thisPosition  = this.getPosition(),
			    otherPosition = otherObject.getPosition(),
			    thisSize      = this.getSize(),
			    otherSize     = otherObject.getSize();

			var xIntersection = (otherPosition.x > thisPosition.x && otherPosition.x < thisPosition.x + thisSize.width) ||
			                    (thisPosition.x > otherPosition.x && thisPosition.x < otherPosition.x + otherSize.width),

			    yIntersection = (otherPosition.y > thisPosition.y && otherPosition.y < thisPosition.y + thisSize.height) ||
			                    (thisPosition.y > otherPosition.y && thisPosition.y < otherPosition.y + otherSize.height);


			return (xIntersection && yIntersection);


		};

		if (item !== undefined) {
			this.register(item);
		}
	};

	GameObject.RenderContext = {
		DISABLED: 0,
		FRONT:    1,
		BACK:     2,
	};

	GameObject.FillMode = {
		NONE:        0,
		REPEAT_X:    1,
		REPEAT_Y:    2,
		REPEAT_ALL:  3,
		FULL_WIDTH:  4,
		FULL_HEIGHT: 5,
		STRETCH:     6
	};
	/*
	 * =======================================================================
	 * =======================================================================
	 * =======================================================================
	 * =======================================================================
	 */


	window.GameCollider = function () {

		var objects = [];

		this.registerObject = function (object, collisionDetectedCallback, collisionMethodFunction) {

			objects.push({
				             object:   object.collidable(),
				             callback: collisionDetectedCallback,
				             method:   collisionMethodFunction
			             });

			return this;
		};

		this.check = function () {

			for (var i = 0; i < objects.length - 1; i++) {

				var thisObject = objects[i];

				if (thisObject.object.canCollide()) {
					if (typeof (thisObject.method) !== "function") thisObject.method = thisObject.object.collidedWith;
					for (var j = i + 1; j < objects.length; j++) {
						var otherObject = objects[j];
						if (otherObject.object.canCollide()) {

							if (thisObject.method.call(thisObject.object, otherObject.object)) {
								thisObject.callback.call(thisObject.object, otherObject.object);

							}

						}


					}

				}
			}

		}


	}

})();

