(function () {

	var DEBUG = false;

	/**
	 * has the latest instance of Game Drawer.
	 *
	 * @type {Window.GameDrawer}
	 */
	var gameDrawer = null;

	window.GameDrawer = function () {

		if (gameDrawer !== null) return gameDrawer;
		gameDrawer = this;

		var gameInstance      = new Game(),
		    size              = gameInstance.getCanvasSize(),
		    canvas            = document.createElement('canvas'),
		    destinationCanvas = null,
		    backbufferContext = null;


		if (!DEBUG) canvas.style.display = 'none';
		document.querySelector('body').appendChild(canvas);


		this.wrapText = function (ctx, text, x, y, maxWidth, lineHeight) {
			var words   = text.split(' ')
				, line  = ''
				, lines = [];

			for (var n = 0, len = words.length; n < len; n++) {
				var testLine    = line + words[n] + ' '
					, metrics   = ctx.measureText(testLine)
					, testWidth = metrics.width;

				if (testWidth > maxWidth) {
					lines.push({text: line, x: x, y: y});
					line = words[n] + ' ';
					y += lineHeight;
				} else {
					line = testLine;
				}
			}

			lines.push({text: line, x: x, y: y});
			return lines;
		};

		this.createCanvas = function (fullscreen, width, height) {

			var theCanvas = document.createElement('canvas');

			if (fullscreen) {
				/*
				 * Get viewport width
				 */
				width = window.innerWidth;
				height = window.innerHeight;

				theCanvas.style.position = 'fixed';
				theCanvas.style.top = 0;
				theCanvas.style.right = 0;
				theCanvas.style.bottom = 0;
				theCanvas.style.left = 0;

			}
			theCanvas.width = width;
			theCanvas.height = height;
			this.setCanvas(theCanvas);
			document.querySelector('body').appendChild(theCanvas);

		};

		/**
		 * Register the destination canvas
		 *
		 * @param {HTMLCanvasElement} canvasObject
		 */
		this.setCanvas = function (canvasObject) {

			destinationCanvas = canvasObject;
			gameInstance.setCanvasSize(canvasObject.width, canvasObject.height);

			canvas.setAttribute('width', canvasObject.width.toString());
			canvas.setAttribute('height', canvasObject.height.toString());
			canvas.width = canvasObject.width;
			canvas.heigh = canvasObject.height;
			backbufferContext = canvas.getContext('2d');
		};

		canvas.setAttribute('width', size.width);
		canvas.setAttribute('height', size.height);

		backbufferContext = canvas.getContext('2d');

		gameInstance.getContext = function (backscreen) {
			return backscreen ? backbufferContext : destinationCanvas.getContext('2d');
		};

		function renderBackfaceObject(fps, image, context) {

			var theObject = this;

			var position       = theObject.getPosition(),
			    size           = theObject.getSize(),
			    // image          = theObject.getObject(),
			    direction      = theObject.getDirection(),
			    frame          = theObject.getCurrentStep(),
			    fillMode       = theObject.getFillMode(),
			    frameDirection = theObject.getFramesDirection().toUpperCase(),
			    offsetX        = size.width * ((frameDirection === 'X') ? frame : direction),
			    offsetY        = size.height * ((frameDirection === 'Y') ? frame : direction),
			    canvasWidth    = backbufferContext.canvas.width,
			    canvasHeight   = backbufferContext.canvas.height;


			// Draw only the items on th ecanvas not the things outside it

			var onCanvas = (position.x >= 0 && position.x < canvasWidth || position.x < 0 && size.width + position.x > 0) &&
			               (position.y >= 0 && position.y < canvasHeight || position.y < 0 && size.height + position.y > 0),
			    canDraw  = ((position.x >= 0 && position.x < canvasWidth) || fillMode === GameObject.FillMode.REPEAT_X) &&
			               ((position.y >= 0 && position.y < canvasHeight) || fillMode === GameObject.FillMode.REPEAT_Y) ||
			               [GameObject.FillMode.FULL_WIDTH, GameObject.FillMode.FULL_HEIGHT, GameObject.FillMode.STRETCH].indexOf(fillMode) !== -1

			               || fillMode === GameObject.FillMode.REPEAT_ALL;
			if (canDraw) {
				context.beginPath();
				if (onCanvas && image) {

					var scale = theObject.getScale();

					if (theObject.getIdentifier() === 'sky') {
						console.log(size);
					}
					switch (fillMode) {
						case GameObject.FillMode.FULL_WIDTH:
							scale  = 100 / size.width * gameInstance.getCanvasSize().width;
							break;
						case GameObject.FillMode.FULL_HEIGHT:
							scale = 100 / size.height * gameInstance.getCanvasSize().height;
							break;
						case GameObject.FillMode.STRETCH:

					}

					context.drawImage(image, offsetX, offsetY, size.width, size.height, position.x, position.y, size.width / 100 * scale, size.height / 100 * scale);

				}
				if (theObject.debugable()) {

					if (DEBUG) console.log("Drawing container rect for ", theObject.getIdentifier());

					context.strokeStyle = '#A00';
					context.lineWidth = 1;
					context.rect(position.x, position.y, size.width / 100 * scale, size.height / 100 * scale);
					context.stroke();
				}


				var positionOffsetX = position.x,
				    positionOffsetY = position.y;



				if ([GameObject.FillMode.REPEAT_ALL, GameObject.FillMode.REPEAT_Y, GameObject.FillMode.REPEAT_X].indexOf(fillMode) !== -1) {

					if (fillMode === GameObject.FillMode.REPEAT_Y || fillMode === GameObject.FillMode.REPEAT_ALL) {
						// Repeating on the X axis
						positionOffsetY = (positionOffsetY % canvasHeight) % size.height - size.height;
					}
					do {
						if (fillMode === GameObject.FillMode.REPEAT_X || fillMode === GameObject.FillMode.REPEAT_ALL) {
							// Repeating on the X axis
							positionOffsetX = (positionOffsetX % canvasWidth) % size.width - size.width;
						}

						do {
							backbufferContext.drawImage(image, offsetX, offsetY, size.width, size.height, positionOffsetX, positionOffsetY, size.width, size.height);
							if (theObject.debugable()) {

								console.log("Drawing container rect for ", theObject.getIdentifier());

								context.strokeStyle = '#A00';
								context.lineWidth = 1;
								context.rect(positionOffsetX, positionOffsetY, size.width, size.height);
								context.stroke();
							}


							positionOffsetX += size.width;
						} while ((fillMode === GameObject.FillMode.REPEAT_X || fillMode === GameObject.FillMode.REPEAT_ALL) && positionOffsetX < canvasWidth);

						positionOffsetY += size.height;
					} while ((fillMode === GameObject.FillMode.REPEAT_Y || fillMode === GameObject.FillMode.REPEAT_ALL) && positionOffsetY < canvasHeight);

				}

			}

		}

		gameInstance.onRegisterObject(function (gameObject) {
			if (gameObject.getRenderContext() !== GameObject.RenderContext.DISABLED) {
				gameObject.setRenderContext(GameObject.RenderContext.BACK);
				gameObject.onRender(renderBackfaceObject.bind(gameObject));

			}
		});

		gameInstance
		/**
		 * Empty the canvas object
		 */
			.onRender(function (fps, context) {


				if (DEBUG) console.log('Clearing backface');
				// context.clearRect(0, 0, context.canvas.width, context.canvas.height);
				context.canvas.width = context.canvas.width;
			}, true)


			/**
			 * Draws all located objects
			 */
			.onRender(function (fps, context) {
				if (DEBUG) console.log('Clearing frontface');
				context.fillStyle = '#fff';
				context.drawImage(backbufferContext.canvas, 0, 0, backbufferContext.canvas.width, backbufferContext.canvas.height);
			});
	}

})();