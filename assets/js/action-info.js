(function () {

	var instance         = null,
	    usedObject = null;

	window.ActionInfo = function () {


		if (instance !== null) return instance;
		instance = this;

		this.use = function (object) {
			usedObject = object;
			return this;
		};

		this.init = function () {

			var game = Game();


			game.createGameObject('action-info')
			    .setPosition(0, -204)
			    .setAllowedSteps(0)
			    .setSize(game.canvas.width, 40)
			    .onRender(function (fps, item, context) {


				    var currentAction = ControlPanel().getCurrentAction(),

				        text          = currentAction ? currentAction.label : '';

				    if (usedObject) {
					    text += ' ' + (usedObject.getProperty('label') || '');
				    }

				    context.fillStyle = '#000';
				    context.rect(this.getPosition().x, this.getPosition().y,
				                 this.getSize().width, this.getSize().height);
				    context.fill();
				    context.fillStyle = '#fff';
				    context.fillText(text, this.getPosition().x + 30, this.getPosition().y + 30);

			    }, false);
		}

	}


})();