(function () {

	var _resources         = [],
	    preloadedResources = [],
	    _loading           = false,
	    instance           = null,
	    _options           = {
		    start:    null,
		    end:      null,
		    progress: null,
		    error:    null
	    };


	function load() {
		if (!_loading && typeof _options.start === 'function') _options.start();


		var resource = _resources.shift();

		if (resource) {
			_loading = true;


			var img = new Image();

			img.onload = function () {
				if (typeof _options.progress === 'function') _options.progress(resource, _resources.length);
				preloadedResources[resource] = img;
				load();
			};

			img.onerror = function () {
				if (typeof _options.error === 'function') _options.error(resource);
				load();
			};

			img.src = resource;

		} else if (_loading) {
			_loading = false;
			if (typeof _options.end === 'function') _options.end();
		}
	}

	window.Preloader = function (options) {

		/*
		 * Gestione del singleton
		 */
		if (instance !== null) return instance;


		_options = options;

		this.enqueue = function (resources) {

			if (typeof resources === 'string') resources = [resources];

			resources.forEach(function (resource) {
				_resources.push(resource);
			});

			if (!_loading) {
				load();
			}

		};


		this.getResource = function (resource) {
			return preloadedResources[resource];
		}
	};


})();