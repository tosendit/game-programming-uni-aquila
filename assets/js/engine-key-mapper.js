(function () {


	window.KeyMapper = function () {

		var bindedCallbacks = {};


		this.bind = function (keyCodeOrChar, downCallback, upCallback) {

			// Convert the char to keycode:

			bindedCallbacks[keyCodeOrChar] = {
				keydown: downCallback,
				keyup:   upCallback,
				invoked: false
			};

			return this;
		};

		this.unbind = function (keyCodeOrChar) {

			delete (bindedCallbacks[keyCodeOrChar]);
			return this;

		};

		function detectBinding(key, code, which) {

			if (typeof bindedCallbacks[code] !== "undefined") {
				return bindedCallbacks[code];
			} else if (typeof bindedCallbacks[key] !== "undefined") {

				return bindedCallbacks[key];

			} else if (typeof bindedCallbacks[which] !== "undefined") {
				return bindedCallbacks[which];
			}

			return undefined;

		}

		function checkBindings(evt) {
			var managedCallbacks = detectBinding(evt.key, evt.code, evt.which);

			if (managedCallbacks) {
				evt.preventDefault();
				evt.stopImmediatePropagation();
				if (evt.type === 'keydown' && !managedCallbacks.invoked) {
					managedCallbacks.invoked = true;
					if (typeof managedCallbacks.keydown === 'function') managedCallbacks.keydown();
				} else if (evt.type === 'keyup') {
					managedCallbacks.invoked = false;
					if (typeof managedCallbacks.keyup === 'function') managedCallbacks.keyup();
				}
			}
		}


		document.addEventListener('keydown', checkBindings);
		document.addEventListener('keyup', checkBindings);

	};

})();