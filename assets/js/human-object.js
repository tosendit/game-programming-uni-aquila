(function () {
	'use strict';

	window.HumanObject = function () {
		var go = new GameObject();

		go.setState = function () {

			go.getObject().setAttribute('data-step', go.getCurrentStep());
			go.getObject().setAttribute('data-step', go.getCurrentStep());
			go.getObject().setAttribute('data-direction', go.getDirection());

		};

		go.start = function (init) {

			go.getObject().classList.add('move');
			if(!go.isPaused()) {
				go.setCurrentStep(0);
			}
		};

		return go;
	};

})();