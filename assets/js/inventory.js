(function () {

	var slots              = [],
	    inventoryBox       = {
		    width:  64,
		    height: 64
	    },
	    fullWidth          = 0,
	    maxHorizontalSlots = 0,
	    maxVisibleSlots    = 0,
	    slotsOffset        = 0,
	    fullHeight         = 132,
	    instance           = null;

	window.GameInventory = function () {


		if (instance !== null) return instance;
		instance = this;

		var theGame = Game();

		function detectAvailableSlots() {

			fullWidth = theGame.getContext().width - (ControlPanel.BUTTON_WIDTH + ControlPanel.MARGIN * 2) * 3;
			maxHorizontalSlots = parseInt(fullWidth / (inventoryBox.width + GameInventory.MARGIN * 2));

			maxVisibleSlots = maxHorizontalSlots * 2;

		}

		this.init = function () {

			game.createGameObject('inventory-arrow-up')
			    .setPosition(-50,-150)
			    .setSize(50, 75)
			    .registerObject( preloader.getResource() );


			detectAvailableSlots();





		}


	};

	window.GameInventory.MARGIN = 5;


})();