(function () {


	window.MouseMapper = function () {

		var bindedCallbacks = {},
		    moveCallback    = null;


		this.bind = function (mouseButton, downCallback, upCallback) {


			// Convert the char to keycode:

			bindedCallbacks[mouseButton] = {
				down:    downCallback,
				up:      upCallback,
				invoked: false
			};

			return this;
		};

		this.unbind = function (mouseButton) {

			delete (bindedCallbacks[mouseButton]);
			return this;

		};

		this.move = function (callback) {
			moveCallback = callback;
		};

		function isButton(which, type, index) {

			var isDown     = (type === 'mousedown'),
			    isUp       = !isDown,
			    isPressed  = (which & index) !== 0,
			    wasPressed = bindedCallbacks[window.MouseMapper.BUTTONS.LEFT].invoked;


			console.log({
				            which:      which,
				            index:      index,
				            isDown:     isDown,
				            isPressed:  isPressed,
				            isUp:       isUp,
				            wasPressed: wasPressed

			            });

			return isDown && isPressed ||
			       isUp && wasPressed && !isPressed;

		}

		function detectBinding(which, type) {

			var hasLeft   = (typeof bindedCallbacks[window.MouseMapper.BUTTONS.LEFT] !== "undefined"),
			    hasRight  = (typeof bindedCallbacks[window.MouseMapper.BUTTONS.RIGHT] !== "undefined"),
			    hasMiddle = (typeof bindedCallbacks[window.MouseMapper.BUTTONS.MIDDLE] !== "undefined"),
			    isLeft    = hasLeft && isButton(which, type, window.MouseMapper.BUTTONS.LEFT),
			    isRight   = hasRight && isButton(which, type, window.MouseMapper.BUTTONS.RIGHT),
			    isMiddle  = hasMiddle && isButton(which, type, window.MouseMapper.BUTTONS.MIDDLE),
			    bindings  = [];


			isLeft && bindings.push(bindedCallbacks[window.MouseMapper.BUTTONS.LEFT]);
			isRight && bindings.push(bindedCallbacks[window.MouseMapper.BUTTONS.RIGHT]);
			isMiddle && bindings.push(bindedCallbacks[window.MouseMapper.BUTTONS.MIDDLE]);

			console.log({
				            left:   isLeft,
				            right:  isRight,
				            middle: isMiddle
			            });

			return bindings;

		}


		function checkBindings(evt) {

			if (checkMovement(evt) === false) return;

			console.log({buttons: evt.buttons, which: evt.which, button: evt.button});
			var managedCallbacksList = detectBinding(evt.buttons, evt.type);

			if (managedCallbacksList.length > 0) {

				evt.preventDefault();
				evt.stopImmediatePropagation();
				event.stopPropagation();


				managedCallbacksList.forEach(function (managedCallbacks) {
					if (evt.type === 'mousedown' && !managedCallbacks.invoked) {
						managedCallbacks.invoked = true;
						if (typeof managedCallbacks.down === 'function') managedCallbacks.down(evt);
					} else if (evt.type === 'mouseup') {
						managedCallbacks.invoked = false;
						if (typeof managedCallbacks.up === 'function') managedCallbacks.up(evt);
					}
				});

			}
		}

		function checkMovement(evt) {
			if (typeof moveCallback === 'function') return moveCallback(evt.offsetX, evt.offsetY, evt.buttons);
		}

		document.addEventListener('mousedown', checkBindings);
		document.addEventListener('mouseup', checkBindings);
		document.addEventListener('mousemove', checkMovement);

		document.addEventListener('contextmenu', function (event) {
			event.preventDefault();
			event.stopImmediatePropagation();
			event.stopPropagation();
		});

	};


	window.MouseMapper.BUTTONS = {
		LEFT:   1,
		RIGHT:  2,
		MIDDLE: 4,
	};

})();